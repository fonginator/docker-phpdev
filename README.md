# Docker PHP Development

This repo contains files that can be used to set up a multi-PHP development environment running under Apache. It currently supports PHP 5.6, 7.1 and 7.2 running in a php-fpm configuration.

It also provides a working [Mailhog](https://github.com/mailhog/MailHog) installation.

**DO NOT USE THIS FOR PRODUCTION.**

**THIS WORKS FOR ME BUT MAY NOT WORK FOR ANYONE ELSE. USE AT YOUR OWN PERIL.**

**DO NOT USE THIS FOR PRODUCTION.**

## Assumptions

Here are a few assumptions we'll be making:

* All of your Git clones (or Subversion checkouts) are grouped together in a single directory on your computer;
* You are using Mac or Linux (Docker works fine on Windows but I'm not familiar enough with it so some of the command-line instructions and paths may be slightly different, and DNS resolver options will differ);
* You know how to edit your hosts file or set up something like dnsmasq ([macOS](https://passingcuriosity.com/2013/dnsmasq-dev-osx/), [Linux](https://leehblue.com/setting-up-dnsmasq-for-local-development/)) or [echoipdns](https://github.com/zapty/echoipdns) for macOS, Linux and Windows;
* Your dev sites will be a subdomain of `.web.devel` (e.g., `myfancydevsite.web.devel`);
* You are already running a MySQL server on your host or somewhere else that is accessible by the containers.[^no-mysql]

[^no-mysql]: This Docker application doesn't set up a MariaDB/MySQL server for you but it would be easy enough to modify it to do so—see the comments in `docker-compose.yml`. Alternatively, you could just install MySQL (or MariaDB) as usual and just configure your projects to use that.

## Prerequisites

You'll need the path to your Git clones/Subversion checkouts on your computer (the host). For example, you might have a folder in your home directory called "work" that contains a number of different projects. Your directory structure might resemble this on a Linux-based system:

<pre>
/home
    /derek
        /work
            /project1
            /project2
            /project3
            /project4
            /project5
            […]
</pre>

Therefore, you will need to mount `/home/derek/work` into the Apache and PHP-FPM Docker containers so that they can access your files.

Once the containers are up and running, your code will be accessible inside those containers at `/var/www/project1`, `/var/www/project2`, etc. That's because we'll mount the local path, `/home/derek/work`, as `/var/www` into those containers. This will be done below.

## Quickstart

I'll try to keep this section as concise as possible so don't skip any steps here.

### Install Docker

Install Docker for your host platform of choice:

[Get Started with Docker](https://www.docker.com/get-started)

### Generate your SSL certificate

`cd` into the top-level directory of this repository.

* `cd apache2/build && ./generate_ssl_cert.sh`

A public/private keypair for `*.web.devel` will be generated and stored in the `ssl` subdirectory.

### Build the Docker images

`cd` into the top-level directory of this repository.

* `cd apache2/build && ./build.sh`
* `cd ../../fpm/build && ./build.sh`

### Configure your environment

`cd` into the top-level directory of this repository.

* `cp .env.sample .env`
* Edit `.env` to set up your environment accordingly. Specifically, modify the `VHOSTS` variable to point to the local path to your source code (see **Prerequisites** above).

### Configure your virtualhosts

`cd` into the top-level directory of this repository.

* `cd apache2/sites-enabled/build`
* `cp vhosts.php.sample vhosts.php`
* Edit `vhosts.php`, configure virtualhosts via the `$vhosts` array (note that `document_root` refers to the path inside the Docker container not the path on your host, so the paths should always start with `/var/www`)
* Rebuild virtualhost fragments by executing the script like so: `./build-sites-enabled.php` (the generated config file fragments are "imported" into the Docker container when you start it up)

See the examples/documentation in the vhosts file for configuration examples, including how to specify the version of PHP each of your virtualhosts will use.

### Launch containers

`cd` into the top-level directory of this repository (where you will find the `docker-compose.yml` file) then do `docker-compose up`.

If all went well, there should be no warnings or errors, and you'll probably see something like this:

<pre>
fong@Tolstoy:Docker/docker-phpdev $ docker-compose up
mailhog is up-to-date
Creating phpfpm-5.6 ... done
Creating phpfpm-7.2 ... done
Creating apache2    ... done
Attaching to mailhog, phpfpm-5.6, phpfpm-7.2, apache2
phpfpm-5.6    | [20-Feb-2019 11:32:29] NOTICE: fpm is running, pid 1
phpfpm-7.2    | [20-Feb-2019 11:32:29] NOTICE: fpm is running, pid 1
phpfpm-5.6    | [20-Feb-2019 11:32:29] NOTICE: ready to handle connections
phpfpm-7.2    | [20-Feb-2019 11:32:29] NOTICE: ready to handle connections
mailhog       | 2019/02/20 16:32:08 Using in-memory storage
mailhog       | 2019/02/20 16:32:08 [SMTP] Binding to address: 0.0.0.0:1025
mailhog       | 2019/02/20 16:32:08 Serving under http://0.0.0.0:8025/
mailhog       | [HTTP] Binding to address: 0.0.0.0:8025
mailhog       | Creating API v1 with WebPath: 
mailhog       | Creating API v2 with WebPath: 
apache2       | [Wed Feb 20 16:32:30.922759 2019] [ssl:warn] [pid 1:tid 140181197060968] AH01909: www.example.com:443:0 server certificate does NOT include an ID which matches the server name
apache2       | [Wed Feb 20 16:32:30.941656 2019] [ssl:warn] [pid 1:tid 140181197060968] AH01909: www.example.com:443:0 server certificate does NOT include an ID which matches the server name
apache2       | [Wed Feb 20 16:32:30.943349 2019] [mpm_event:notice] [pid 1:tid 140181197060968] AH00489: Apache/2.4.38 (Unix) OpenSSL/1.1.1a configured -- resuming normal operations
apache2       | [Wed Feb 20 16:32:30.943384 2019] [core:notice] [pid 1:tid 140181197060968] AH00094: Command line: 'httpd -D FOREGROUND'
</pre>

You can generally ignore the warnings in the output above—they will not affect the functionality of your containers.

### Mailhog

You should also be able to access Mailhog at [http://127.0.0.1:8025](http://127.0.0.1:8025). All email sent by your virtualhosts (via PHP's `mail` function or when using SMTP to localhost port 1025) should be intercepted by Mailhog and accessible at this web interface.

For best results, make sure your projects' SMTP configurations use **localhost port 1025** so that everything gets sent through Mailhog. This ensures that no email will accidentally get sent to external email addresses as part of your development.

The configuration of the PHP-FPM instances will automatically use Mailhog when sending mail via PHP's `mail` function.

### Project database configuration

Assuming your MySQL server is running on localhost (your computer), you should make sure your database connection strings use `host.docker.internal` instead of `localhost` or `127.0.0.1` as the hostname. **This is a special hostname that Docker creates inside its containers that refers back to the host machine under which it is running.**

### Shut down the containers

When you're done working, you can use `docker-compose down` in the same directory where you issued the `docker-compose up` command to shutdown and destroy the running containers.

## DNS configuration

When your Docker containers are running, your virtualhosts are accessible at localhost (127.0.0.1). However, you still need to be able to map the domain names of your virtualhosts to 127.0.0.1 so that the correct sites come up in your web browser.

You can accomplish this with dnsmasq (macOS, Linux) or echoipdns (macOS, Linux, Windows). Once configured for your host platform, these applications will let you map `*.web.devel` to `127.0.0.1`.

Docker images also exist for dnsmasq (see [https://blog.csainty.com/2016/09/running-dnsmasq-in-docker.html](https://blog.csainty.com/2016/09/running-dnsmasq-in-docker.html) for an example) in case you're not comfortable installing dnsmasq and its dependencies directly on your computer.

As a last resort, you can also just make entries in your hosts file if you don't mind having to manually manage these entries every time you add, edit or remove a virtualhost. Unlike dnsmasq, your hosts file works with specific hostnames; dnsmasq is more convenient because you can create a wildcard entry (`*.web.devel`) once that maps to your localhost.

## Checkpoint

Assuming your DNS is set up and your containers are running, you should be able to access your configured virtualhosts in your web browser (e.g., https://myphp5project.web.devel).

## Day to day workflow

With everything set up, your day to day workflow will be quite simple:

* `cd` into the top-level directory of this project
* `docker-compose up` or `docker-compose start` to start your containers
* `docker-compose down` or `docker-compose stop` when you're done working

Both `down` and `stop` will stop the running containers, but the former will also destroy them along with any networks Docker created. This also means that containers will get rebuilt the next time you do a `docker-compose up`. Use whatever feels better to you.

## Updating Apache virtualhosts

To add, edit or delete virtualhosts, you simply need to edit the PHP file at `apache2/sites-enabled/build/vhosts.php`, make your changes, then execute the script at the same location to regenerate your Apache virtualhost fragments: `./build-sites-enabled.php`

If your containers are already running, `cd` back to the top of the project repository, then destroy the running containers and start them up again:

`docker-compose down && docker-compose up`

## FAQ

### What is Docker?

Generally, Docker is a lightweight, container-based virtualization solution. It emphasizes the use of containers that are limited to specific tasks (an Apache container just serving web files, a MySQL server only running MySQL, etc.). Docker is much more lightweight than running virtual machines using software like Virtualbox, VMWare or Parallels. Containers are ephemeral in nature—they are meant to be destroyed and rebuilt on demand which makes them an ideal solution for ensuring that environments are always operating under consistent conditions.

### Why Apache and not nginx?

Because we use Apache in staging and production but feel free to replace with nginx or some other web server of choice. It shoudn't matter except when it comes to rewrite rules which are generally configured in `.htaccess` files which nginx doesn't support. (So maybe just stick with Apache…)

## Further reading

The Docker website has a lot of good information - [https://www.docker.com](https://www.docker.com)
