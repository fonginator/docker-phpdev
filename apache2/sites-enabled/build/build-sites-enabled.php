#!/usr/bin/env php
<?php
error_reporting(E_ALL);

require_once 'vhosts.php';

define('VHOST_TEMPLATES_DIR', __DIR__ .'/vhost_templates');
define('VHOST_DEFAULT_TEMPLATE', 'standard.conf.tmpl');

// define virtualhost conf templates available for use
$vhostTemplates = array(
    'standard.conf.tmpl',
);

if (!in_array(VHOST_DEFAULT_TEMPLATE, $vhostTemplates)) {
    // default template must be defined in $vhostTemplates
    echo "Invalid default template ". VHOST_DEFAULT_TEMPLATE ."\n";
    exit(1);
}

// make sure all defined templates are readable
foreach ($vhostTemplates as $template) {
    $file = VHOST_TEMPLATES_DIR .'/'. $template;
    
    if (!is_readable($file)) {
        echo "Unable to read ". $file ."\n";
        exit(1);
    }
}

// stats and reporting
$generated = 0;
$errors = array();

foreach ($vhosts as $vhost => $props) {
    if (!isset($props['vhost_template'])) {
        $props['vhost_template'] = VHOST_DEFAULT_TEMPLATE;
    }

    if (!isset($props['server_alias'])) {
        $props['server_alias'] = array('www.'. $vhost);
    } elseif (!is_array($props['server_alias'])) {
        $props['server_alias'] = array($props['server_alias']);
    }
    
    if (!isset($props['vhost_block_end'])) {
        $props['vhost_block_end'] = null;
    }
    
    $templateFile = VHOST_TEMPLATES_DIR .'/'. $props['vhost_template'];
    $contents = file_get_contents($templateFile);

    // do a simple search and replace
    $contents = str_replace(array(
        '%%SERVER_NAME%%',
        '%%SERVER_ALIAS%%',
        '%%DOCUMENT_ROOT%%',
        '%%PHPFPM_HANDLER%%',
        '%%VHOST_BLOCK_END%%',
    ), array(
        $vhost,
        implode(' ', $props['server_alias']),
        $props['document_root'],
        $props['phpfpm_handler'],
        $props['vhost_block_end'],
    ), $contents);
    
    $target = __DIR__ .'/../'. $vhost .'.conf';
    if (file_put_contents($target, $contents)) {
        $generated += 1;
    } else {
        $errors[] = $vhost;
    }
}

echo "Total vhosts: ". count($vhosts) ."\n";
echo "Total conf files generated: ". $generated ."\n";

if (!empty($errors)) {
    echo "Conf files could not be generated for the following vhosts:\n";
    foreach ($errors as $vhost) {
        echo " - ". $vhost ."\n";
    }
}

exit(0);
