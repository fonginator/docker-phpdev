#!/usr/bin/env bash

# target directory where SSL certificate will be saved
dir="./ssl"

function generate_certificate {
    if [[ ! -e $dir ]]; then
        mkdir $dir
    fi
    
    openssl req -x509 -sha256 -new -newkey rsa:4096 -nodes -subj '/CN=*.web.devel' -keyout $dir/wildcard.web.devel.key -out $dir/wildcard.web.devel.crt -days 3650
}

printf "This script generates a self-signed certificate that will be imported into your Docker image.\n"
printf "If you have already an SSL certificate in the %s subdirectory, there should be no need to rerun this.\n" $dir
echo "This will overwrite any existing certificate you may have in the ssl subdirectory. Continue?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) generate_certificate ; break;;
        No ) exit;;
    esac
done
