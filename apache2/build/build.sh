#!/usr/bin/env bash

IMAGE_NAME="apache2"

# confirm docker daemon is running and connected
docker version

# build the image based on the Dockerfile and name it
docker build -t $IMAGE_NAME .

# confirm image is present
docker images
