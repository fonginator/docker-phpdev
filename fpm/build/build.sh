#!/usr/bin/env bash

# define php-fpm versions to build
versions=( "5.6" "7.1" "7.2" "7.3" "8.2" )

# confirm docker daemon is running and connected
docker version

# build our images and tag them accordingly
for version in "${versions[@]}"
do
    PHPVER=$version
    FROM_IMAGE="php:${version}-fpm"
    IMAGE_NAME="phpfpm-${version}"

    docker build -f Dockerfile-$PHPVER --build-arg FROM_IMAGE=$FROM_IMAGE --build-arg PHPVER=$PHPVER -t $IMAGE_NAME .
done    

# confirm image is present
docker images
